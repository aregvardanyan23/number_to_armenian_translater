from nums import digits_single, digits_double


class NumToArm:
    def __init__(self, num):
        if type(num) != int:
            raise ValueError("Type is not int")
        self.num = str(num)

    def single(self, num):
        return digits_single[num[-1]]

    def double(self, num):
        if len(str(num)) > 1:
            if num[-1] != '0':
                return digits_double[num[-2]] + 'ն' + self.single(num[-1])
            elif num[-2] == 0:
                return digits_double[num[-2]] + 'ը'
            else:
                return digits_double[num[-2]] + 'ն'
        return self.single(num)

    def hundred(self, num):
        if len(str(num)) > 2:
            if num[-3] == '1':
                return ' հարյուր ' + self.double(num[-2:])
            return digits_single[num[-3]] + ' հարյուր ' + self.double(num[-2:])
        return self.double(num)

    def thousand(self, num):
        if len(str(num)) > 3:
            if num[-4] == '1':
                return ' հազար ' + self.hundred(num[-3:])
            return self.hundred(num[:-3]) + ' հազար ' + self.hundred(num[-3:])
        return self.hundred(num)

    def million(self, num):
        if len(str(num)) > 6:
            return self.hundred(num[:-6]) + ' միլիոն ' + self.thousand(num[-6:])
        return self.thousand(num)

    def translate(self):
        if len(str(self.num)) < 10:
            return self.million(self.num)
        return "Num is so large"


num1 = NumToArm(1111)
print(num1.translate())
